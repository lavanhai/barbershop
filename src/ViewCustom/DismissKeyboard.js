import React from "react";
import { Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback } from "react-native";

export function DismissKeyboard({ children }) {
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView
        behavior={0}
        style={[{ flex: 1, justifyContent: "center" }]}
      >
        {children}
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}
