import React from "react";
import FlashMessage from "react-native-flash-message";
import { StatusBar } from "react-native";

export function FlashMessageView({ refId }) {
  return <FlashMessage
    ref={refId}
    position="top"
    style={{ paddingTop: StatusBar.currentHeight}}
    animated={true}
    autoHide={true}
  />;
}
