import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default function Header(props) {
  const navigation = useNavigation();
  return (
    <View>
      <SafeAreaView style={{ backgroundColor: "white" }}>
        <StatusBar translucent backgroundColor="transparent" barStyle={"dark-content"} />
      </SafeAreaView>
      <Card
        style={{
          flexDirection: "row",
          borderWidth: 0,
          justifyContent: "space-between",
          paddingTop: StatusBar.currentHeight,
          ...props.style,
        }}>
        <View style={{ flexDirection: "row", alignItems: "center", flex: 1, paddingVertical:10, paddingLeft: 8}}>
          {
            !props.onBack ? <View />
              :
              <TouchableOpacity
                onPress={props.onBack ? props.onBack : () => navigation.goBack()}
              >
                <Icon name={"arrow-left"} size={30} color={"black"} />
              </TouchableOpacity>
          }
          <Text style={{ color: "black", fontSize: 16, marginLeft:10 }} numberOfLines={1}>
            {props.title}
          </Text>
        </View>
        {props.right}
      </Card>
    </View>
  );
}

export function Card(props) {
  let shadowOffset = props.shadowOffset ? props.shadowOffset : 8;
  return <View style={{
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "rgba(158, 150, 150, 0.09)",
    borderWidth: 1,
    shadowOffset: { width: shadowOffset, height: shadowOffset },
    shadowColor: "black",
    shadowOpacity: 0.08,
    elevation: 5,
    ...props.style,
  }}>
    {props.children}
  </View>;
}
