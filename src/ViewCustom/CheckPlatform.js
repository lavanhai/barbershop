import {Platform} from 'react-native';

export function isIos() {
    return Platform.OS === 'ios';
}
export function isAndroid() {
    return Platform.OS === 'android';
}
export function isIpad() {
    return Platform.isPad;
}
