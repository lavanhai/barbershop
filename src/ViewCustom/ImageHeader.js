import React from "react";
import { ImageBackground, SafeAreaView, StatusBar, TouchableOpacity, View } from "react-native";
import Images from "../Define/Images";
import { isIos } from "./CheckPlatform";
import { useIsFocused, useNavigation } from "@react-navigation/native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { WIDTH_SCREEN } from "../Define/const";

export function ImageHeader(props) {
  let paddingTop = isIos() ? 0 : StatusBar.currentHeight;
  const navigation = useNavigation();

  function FocusAwareStatusBar(props) {
    const isFocused = useIsFocused();

    return isFocused ? <StatusBar {...props} /> : null;
  }

  return (
    <ImageBackground
      style={[{height: 250}, props.style]}
      source={Images.background}>
      <SafeAreaView style={{ paddingTop: paddingTop }}>
        <FocusAwareStatusBar translucent backgroundColor="transparent" barStyle={props.barStyle} />
      </SafeAreaView>
      {
        !props.onPress ? <View />
          :
          <TouchableOpacity
            style={{ paddingLeft: 8, flexDirection: "row", alignItems: "center" }}
            onPress={props.onPress ? props.onPress : () => navigation.goBack()}
          >
            <Icon name={"arrow-left"} size={28} color={"white"} />
          </TouchableOpacity>
      }
    </ImageBackground>
  );
}
