import { Color } from "../Common/Color";
import { Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import React from "react";
import { WIDTH_SCREEN } from "../Define/const";

export function Button(props) {
  return (
    <TouchableOpacity
      {...props}
      style={{
        alignItems: "center",
        backgroundColor: Color.button,
        width: WIDTH_SCREEN / 2,
        height: 50,
        justifyContent: "center",
        borderRadius: 30,
      }}
    >
      <Text style={{ color: "white" }}>{props.text}</Text>
    </TouchableOpacity>
  );
}
