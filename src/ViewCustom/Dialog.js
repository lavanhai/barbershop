import React from "react";
import {
  Alert,
} from "react-native";

export default function showDialog(title, message, onPress) {
  return Alert.alert(
    title,
    message,
    [
      {
        text: "Không",
        style: "cancel",
      },
      {
        text: "Có",
        onPress: () => {
          onPress();
        },
      },
    ],
    { cancelable: false },
  );
}
