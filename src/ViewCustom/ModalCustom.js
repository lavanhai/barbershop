import { TextInput, TouchableOpacity, View } from "react-native";
import TextView from "../Component/ViewUtils/TextView";
import { Color } from "../Common/Color";
import { WIDTH_SCREEN } from "../Define/const";
import Modal from "react-native-modal";
import React from "react";

export default function ModalCustom(props) {
  return (
    <Modal
      avoidKeyboard={true}
      animationIn="zoomInDown"
      animationOut="zoomOutUp"
      backdropOpacity={0.2}
      backdropColor={"#253b5a"}
      isVisible={props.isVisible}
      onBackdropPress = {props.onBackdropPress}
      {...props}
    >
      <View style={{
        backgroundColor: "#fff",
        width: "100%",
        paddingVertical: 20,
        borderRadius: 14,
      }}>
        <View style={{ marginBottom: 20, justifyContent: 'center', alignItems: 'center' }}>
          <TextView text={props.title.toUpperCase()} style={{ fontWeight: "bold", fontSize: 16 }} />
        </View>
        {props.children}
      </View>
    </Modal>
  );
}

//demo
// <ModalCustom
//   isVisible={true}
//   title={'header'}
//   onBackdropPress={()=>setIsVisible(false)}
// >
//   <View>
//     <TextView text={'hello'} />
//   </View>
// </ModalCustom>
