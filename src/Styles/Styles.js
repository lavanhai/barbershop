import { Color } from "../Common/Color";

export default {
  itemShadow: {
    shadowOffset: {width: 1, height: 1},
    shadowColor: Color.black,
    shadowOpacity: 0.08,
    elevation: 2,
  },
};
