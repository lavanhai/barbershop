import React from "react";
import { FlatList } from "react-native";
import ItemView from "../ViewUtils/ItemView";
import { useNavigation } from "@react-navigation/native";
import store from "../../Redux/Store";
import { API } from "../../Common/API";
import { setDataHair } from "../../Redux/Actions/hairAction";
import { flashMessageShowSuccess } from "../../FunctionCustom/FlashMessageShow";

export function ListHair(props) {
  const navigation = useNavigation();
  if (props.data && props.data.length > 0) {
    return (
      <FlatList
        data={props.data}
        style={props.style}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <ItemView
              image={{ uri: item.image }}
              name={item.name}
              onPress={() => navigation.navigate("Oder", { hair: item })}
              textButton={"Đặt lịch"}
              titleStatus={"Giá: " + item.price + " VND"}
            />
          );
        }}
      />
    );
  }
}

export function updateHair(id_hair, params) {
  let dataAllHair = store.getState().hair.dataHair;
  fetch(API.hairs + `/${id_hair}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(params),
  }).then((response) => response.json())
    .then((res) => {
      if (res.id) {
        dataAllHair.forEach((hair, index) => {
          if (res.id == hair.id) {
            dataAllHair.splice(index, 1, res);
            store.dispatch(setDataHair(dataAllHair.slice()));
            flashMessageShowSuccess('Cập nhật thành công')
          }
        });
      }
    });
}
