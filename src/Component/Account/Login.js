import { Keyboard, Text, TextInput, View } from "react-native";
import React, { useState } from "react";
import {
  flashMessageShowError,
  flashMessageShowSuccess,
  flashMessageShowWarning,
} from "../../FunctionCustom/FlashMessageShow";
import Header from "../../ViewCustom/Header";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Button } from "../../ViewCustom/Button";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { setUser } from "../../Redux/Actions/userAction";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ACCOUNT } from "../../Define/const";

export default function Login() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const dataAllUser = useSelector(state => state.user.dataAllUser);
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View>
      <Header title={"Đăng nhập"} />
      <View style={{ margin: 10 }}>
        <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
          <View style={{ flex: 3 }}>
            <Text>Số điện thoại</Text>
          </View>
          <TextInput
            keyboardType="numeric"
            onChangeText={(text) => setPhone(text)}
            placeholder={"Nhập số điện thoại"}
            style={{ height: 40, flex: 7 }}
          />
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", borderBottomWidth: 0.3 }}>
          <View style={{ flex: 3 }}>
            <Text>Mật khẩu</Text>
          </View>
          <TextInput
            onChangeText={(text) => setPassword(text)}
            secureTextEntry={true}
            placeholder={"Mật khẩu"}
            style={{ height: 40, flex: 7 }}
          />
        </View>
        <View style={{ alignItems: "flex-end", marginTop: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.navigate("Register")}
            style={{ padding: 10 }}>
            <Text style={{ fontWeight: "bold" }}>Đăng ký</Text>
          </TouchableOpacity>
        </View>
        <View style={{ alignItems: "center", marginTop: 30 }}>
          <Button
            onPress={() => handleLogin()}
            text={"Đăng nhập"}
          />
        </View>
      </View>
    </View>
  );

  function handleLogin() {
    Keyboard.dismiss();
    if (phone.trim().length <= 0 || password.trim().length <= 0) {
      flashMessageShowError("Vui lòng nhập tên tài khoản và mật khẩu.");
      return;
    }
    if (dataAllUser) {
      let check = false;
      let data_user = null;
      dataAllUser.forEach(user => {
        if (user.phone == phone && user.password == password) {
          check = true;
          data_user = user;
        }
      });
      if (check) {
        dispatch(setUser(data_user));
        AsyncStorage.setItem(ACCOUNT, JSON.stringify(data_user));
        return navigation.replace("NavTab");
      } else {
        return flashMessageShowError("Tên tài khoản hoặc mật khẩu không đúng!");
      }
    } else {
      return flashMessageShowWarning("Bạn chưa đăng ký tài khoản");
    }
  }
}
