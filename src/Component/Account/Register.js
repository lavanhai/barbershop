import { Keyboard, Text, TextInput, View } from "react-native";
import React, { useState } from "react";
import {
  flashMessageShowError,
  flashMessageShowSuccess,
  flashMessageShowWarning,
} from "../../FunctionCustom/FlashMessageShow";
import Header from "../../ViewCustom/Header";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Button } from "../../ViewCustom/Button";
import { useNavigation } from "@react-navigation/native";
import { API } from "../../Common/API";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ACCOUNT } from "../../Define/const";
import { useDispatch } from "react-redux";
import { setUser } from "../../Redux/Actions/userAction";

export default function Register() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");

  return (
    <View>
      <Header title={"Đăng ký"} />
      <View style={{ margin: 10 }}>
        <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
          <View style={{ flex: 3 }}>
            <Text>Số điện thoại</Text>
          </View>
          <TextInput
            keyboardType={'numeric'}
            onChangeText={(text) => setPhone(text)}
            placeholder={"Nhập số điện thoại"}
            style={{ height: 40, flex: 7 }}
          />
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
          <View style={{ flex: 3 }}>
            <Text>Họ tên</Text>
          </View>
          <TextInput
            onChangeText={(text) => setName(text)}
            placeholder={"Nhập họ tên"}
            style={{ height: 40, flex: 7 }}
          />
        </View>

        <View style={{ flexDirection: "row", alignItems: "center", borderBottomWidth: 0.3 }}>
          <View style={{ flex: 3 }}>
            <Text>Mật khẩu</Text>
          </View>
          <TextInput
            onChangeText={(text) => setPassword(text)}
            secureTextEntry={true}
            placeholder={"Mật khẩu"}
            style={{ height: 40, flex: 7 }}
          />
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", borderBottomWidth: 0.3 }}>
          <View style={{ flex: 3 }}>
            <Text>Nhập lại mật khẩu</Text>
          </View>
          <TextInput
            onChangeText={(text) => setRePassword(text)}
            secureTextEntry={true}
            placeholder={"Nhập lại mật khẩu"}
            style={{ height: 40, flex: 7 }}
          />
        </View>
        <View style={{ alignItems: "flex-end", marginTop: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ padding: 10 }}>
            <Text style={{ fontWeight: "bold" }}>Quay lại đăng nhập</Text>
          </TouchableOpacity>
        </View>
        <View style={{ alignItems: "center", marginTop: 30 }}>
          <Button
            onPress={() => handleRegister()}
            text={"Đăng ký"}
          />
        </View>
      </View>
    </View>
  );

  function handleRegister() {
    Keyboard.dismiss();
    if (name.trim().length <= 0|| phone.trim().length <= 0 || password.trim().length <= 0 || rePassword.trim().length <= 0) {
      flashMessageShowWarning("Vui lòng nhập đầy đủ thông tin");
      return;
    }
    if (password != rePassword) {
      flashMessageShowWarning("Mật khẩu nhập không giống nhau");
      return;
    }
    fetch(API.user, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        phone: phone,
        name:name,
        password: password,
        permission:2,
        employsId:0
      }),
    }).then((response) => response.json())
      .then((res) => {
        if (res.phone == phone) {
          flashMessageShowSuccess("Đăng ký thành công!");
          AsyncStorage.setItem(ACCOUNT, JSON.stringify(res));
          dispatch(setUser(res));
          navigation.replace("NavTab");
        }
      })
      .catch((error) => flashMessageShowError(error));
  }
}
