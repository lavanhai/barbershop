import { Text, View, Image } from "react-native";
import React, { useEffect } from "react";
import { Color } from "../../Common/Color";
import { Spinner } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ACCOUNT } from "../../Define/const";
import { setAllUser, setUser } from "../../Redux/Actions/userAction";
import { useNavigation } from "@react-navigation/native";
import { useDispatch } from "react-redux";
import { API } from "../../Common/API";
import { setDataHair } from "../../Redux/Actions/hairAction";
import { getDataAllEmploys } from "../Employs/handleEmploy";
import { setOder } from "../../Redux/Actions/oderAction";
import store from "../../Redux/Store";

export default function Splash() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  useEffect(() => {
    getDataAllUser();
    getDataAllEmploys();
    getAllDataHair();
    getAllDataOder();
    AsyncStorage.getItem(ACCOUNT).then(data => {
      if (data) {
        let dataUser = JSON.parse(data);
        dispatch(setUser(dataUser));
        navigation.replace("NavTab");
      } else {
        navigation.replace("Login");
      }
    });
  }, []);
  return (
    <View style={{ flex: 1, backgroundColor: Color.main }}>
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Image
          style={{ width: 150, height: 150 }}
          source={require("../../Asset/barber.jpg")} />
        <Text style={{ fontSize: 38, color: "#00657e" }}>
          BARBER SHOP VŨ TÔ
        </Text>
        <Spinner />
      </View>

    </View>
  );

  function getAllDataHair() {
    fetch(API.hairs, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => response.json())
      .then((res) => {
        if (res && res.length > 0) {
          dispatch(setDataHair(res));
        }
      });
  }
}

export function getAllDataOder() {
  fetch(API.oders, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  }).then((response) => response.json())
    .then((res) => {
      if (res && res.length > 0) {
        store.dispatch(setOder(res));
      }
    });
}
export function getDataAllUser() {
  fetch(API.user, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  }).then((response) => response.json())
    .then((res) => {
      if (res && res.length > 0) {
        store.dispatch(setAllUser(res));
      }
    });
}

