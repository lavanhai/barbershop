import React from "react";
import {Text} from "react-native";
import PropTypes from "prop-types";

function TextView(props) {
  return (
    <Text
      {...props}
      style={{
        fontSize : 14,
        ...props.style
      }}>
      {props.text}
    </Text>
  )
}

TextView.propTypes = {
  style: PropTypes.style,
  text: PropTypes.string,
};

export default TextView;
