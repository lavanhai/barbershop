import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import React from "react";
import TextView from "../ViewUtils/TextView";
import { WIDTH_SCREEN } from "../../Define/const";

export default function ItemView(props) {
  return (
    <View style={{ alignItems: "center" }}>

      <View
        style={{ borderRadius: 16, width: "90%", backgroundColor: "white", marginVertical: 5 }}
      >
        <View
          style={{ paddingHorizontal: 10, flexDirection: "row", paddingVertical: 10 }}>
          <Image
            style={{ width: 88, height: 88, borderRadius: 20 }}
            source={props.image} />
          <View
            style={{ paddingLeft: 20, justifyContent: "space-around", width: "50%" }}
          >
            <TextView
              style={{ fontWeight: "700", fontSize: 18 }}
              text={props.name} />
            <View style={{width: WIDTH_SCREEN/2}}>
              <TextView
                style={{ fontSize: 16}}
                text={props.titleStatus} />
              {props.sumOder && props.sumMoney ?
                <View>
                  <TextView
                    style={{ fontSize: 16}}
                    text={props.sumOder} />
                  <TextView
                    style={{ fontSize: 16}}
                    text={props.sumMoney} />
                </View> : null}
            </View>
            {
              !props.onPress ? <View />  :
              <TouchableOpacity
                onPress={props.onPress}
                style={styles.btnView}

              >
                <TextView
                  style={{ color: "#005058" }}
                  text={props.textButton}
                />
              </TouchableOpacity>
            }
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  btnView: {
    borderRadius: 12,
    borderWidth: 1,
    alignItems: "center",
    padding: 2,
    borderColor: "#63C2D1",
    width: "80%",
  },
});

