import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import TextView from "./TextView";

export function ItemMenu(props){
  return  <TouchableOpacity
    onPress={props.onPress}
    style = {{flexDirection: 'row', alignItems: 'center', paddingBottom: 10}}
  >
    <Icon name={props.icon} size={30} />
    <TextView
      style = {{marginLeft: 5}}
      text={props.text} />
  </TouchableOpacity>
}
