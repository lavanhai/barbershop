import React from "react";
import { FlatList, View } from "react-native";
import ItemView from "../ViewUtils/ItemView";

export function ListEmploys(props) {
  let address = props.address;
  let allEmploys = props.data;
  let data = allEmploys;
  if (address != 0 && allEmploys) {
    data = allEmploys.filter(value => value.address == address);
  }
  if (data && data.length > 0) {
    return (
      <View>
        <FlatList
          data={data}
          keyExtractor={(item) => item.id}
          renderItem={({ item, index }) => {
            return (
              <ItemView
                image={{ uri: item.image }}
                name={item.name}
                onPress={() => props.onPress(item)}
                textButton={"Chi Tiết"}
                titleStatus={'Like: ' + item.like}
              />
            );
          }}
        />
      </View>
    );
  }
  return <View />;
}
