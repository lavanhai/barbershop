import { API } from "../../Common/API";
import { setAllEmploys } from "../../Redux/Actions/employsAction";
import store from "../../Redux/Store";
import { setDataHair } from "../../Redux/Actions/hairAction";
import { flashMessageShowSuccess } from "../../FunctionCustom/FlashMessageShow";

export function getDataAllEmploys() {
  fetch(API.employs, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  }).then((response) => response.json())
    .then((res) => {
      if (res && res.length > 0) {
        store.dispatch(setAllEmploys(res));
      }
    });
}

export function getEmployByID(id_employs) {
  let dataAllEmploys = store.getState().employs.dataAllEmploys;
  let employs = dataAllEmploys.filter(value => value.id == id_employs)[0];
  if (employs) {
    return employs;
  }
  return [];
}

export function updateEmploys(id_employs, params) {
  let dataAllEmploys = store.getState().employs.dataAllEmploys;
  fetch(API.employs + `/${id_employs}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(params),
  }).then((response) => response.json())
    .then((res) => {
      if (res.id) {
        dataAllEmploys.forEach((employs, index) => {
          if (res.id) {
            dataAllEmploys.splice(index, 1, res);
            store.dispatch(setAllEmploys(dataAllEmploys.slice()));
            // flashMessageShowSuccess("Thành công");
          }
        });
      }
    });
}
