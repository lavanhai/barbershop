import React, { useEffect, useState } from "react";
import { FlatList, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import TextView from "../ViewUtils/TextView";
import { useNavigation } from "@react-navigation/native";
import { ImageHeader } from "../../ViewCustom/ImageHeader";
import { WIDTH_SCREEN } from "../../Define/const";
import { Color } from "../../Common/Color";
import { updateHair } from "../Hair/ListHair";
import { useDispatch, useSelector } from "react-redux";
import Styles from "../../Styles/Styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { API } from "../../Common/API";
import { updateEmploys } from "../../Redux/Actions/employsAction";
import { getEmployByID } from "./handleEmploy";
import { flashMessageShowError, flashMessageShowWarning } from "../../FunctionCustom/FlashMessageShow";
import ItemView from "../ViewUtils/ItemView";
import ModalCustom from "../../ViewCustom/ModalCustom";

export function DetailEmploys(props) {
  const { employs, key } = props.route.params;
  const dispatch = useDispatch();
  const dataHair = useSelector(state => state.hair.dataHair);
  const dataUser = useSelector(state => state.user.dataUser);
  const allEmploys = useSelector(state => state.employs.dataAllEmploys);
  const [dataEmploys, setDataEmploys] = useState(null);
  const [loading, setLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [nameHair, setNameHair] = useState("");
  const [priceHair, setPriceHair] = useState("");
  const [hairUpdate, setHairUpdate] = useState(null);
  const navigation = useNavigation();


  useEffect(() => {
    setDataEmploys(getEmployByID(employs.id));
  }, [allEmploys]);

  function checkUserLike() {
    let user_like = JSON.parse(dataEmploys.user_like);
    if (user_like.length > 0) {
      if (user_like.indexOf(parseInt(dataUser.id)) != -1) {
        return true;
      }
    }
    return false;
  }

  function handleLike() {
    if (!loading) {
      setLoading(true);
      let user_like = JSON.parse(dataEmploys.user_like);
      let like = dataEmploys.like;
      if (checkUserLike()) {
        let index = user_like.indexOf(parseInt(dataUser.id));
        user_like.splice(index, 1);
        like -= 1;
      } else {
        user_like.push(parseInt(dataUser.id));
        like += 1;
      }
      updateUserLike(user_like, like, dataEmploys.id);
    }
  }


  if (dataEmploys) {
    return (
      <View style={{ flex: 1 }}>
        <ImageHeader
          onPress={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <View style={{ marginBottom: 5, flexDirection: "row", justifyContent: "space-around" }}>
            <View style={{ flexDirection: "row", marginBottom: 5, marginLeft: 20 }}>
              <Image
                style={styles.imageAvt}
                source={{ uri: dataEmploys.image }} />
              <View>
                <TextView
                  style={{ fontSize: 20, marginLeft: 10, fontWeight: "700" }}
                  text={dataEmploys.name} />
                <TextView
                  style={{ fontSize: 16, marginLeft: 10 }}
                  text={"Like: " + dataEmploys.like} />
              </View>

            </View>
            {!key ?
              <TouchableOpacity
                onPress={() => handleLike()}
                style={{
                  backgroundColor: "white",
                  width: WIDTH_SCREEN / 10,
                  aspectRatio: 1,
                  ...Styles.itemShadow,
                  borderRadius: WIDTH_SCREEN / 10 * 2,
                  marginTop: -20,
                  alignItems: "center",
                  justifyContent: "center",

                }}>
                <Icon
                  color={getColorHeart()}
                  size={24}
                  name={"heart-circle"}
                />
              </TouchableOpacity> : null}
          </View>

          <FlatList
            data={dataHair}
            style={{ flex: 1 }}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => {
              return (
                <ItemView
                  image={{ uri: item.image }}
                  name={item.name}
                  onPress={() => onPressItemHair(item)}
                  textButton={key == "admin" ? "Sửa" : "Đặt lịch"}
                  titleStatus={"Giá: " + item.price + " VND"}
                />
              );
            }}
          />
          <ModalCustom
            title={"Sua thong tin"}
            isVisible={isVisible}
            onBackdropPress={() => setIsVisible(false)}
          >
            <View style={{ paddingHorizontal: 10 }}>
              <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
                <View style={{ flex: 3 }}>
                  <Text>Tên kiểu tóc</Text>
                </View>
                <TextInput
                  value={nameHair}
                  onChangeText={(text) => setNameHair(text)}
                  placeholder={"Nhập tên kiểu tóc"}
                  style={{ height: 40, flex: 7 }}
                />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
                <View style={{ flex: 3 }}>
                  <Text>Giá</Text>
                </View>
                <TextInput
                  value={priceHair}
                  onChangeText={(text) => setPriceHair(text)}
                  placeholder={"Nhập giá"}
                  style={{ height: 40, flex: 7 }}
                />
              </View>
              <View
                style={{ alignItems: "center", marginTop: 30, flexDirection: "row", justifyContent: "space-evenly" }}>
                <TouchableOpacity
                  onPress={() => setIsVisible(false)}
                  style={styles.btn}>
                  <TextView text={"Hủy"} style={{ color: "white" }} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => saveUpdateHair()}
                  style={styles.btn}>
                  <TextView text={"Lưu lại"} style={{ color: "white" }} />
                </TouchableOpacity>
              </View>
            </View>
          </ModalCustom>
        </View>
      </View>
    );
  } else {
    return <View />;
  }

  function saveUpdateHair() {
    if (nameHair.length == 0 || priceHair.length == 0) {
      return flashMessageShowWarning("Dữ liệu không được để trống");
    }
    let params = {
      name: nameHair,
      price: priceHair,
    };
    updateHair(hairUpdate.id, params);
    setIsVisible(false);
  }

  function onPressItemHair(item) {
    if (key == "admin") {
      setNameHair(item.name);
      setPriceHair(item.price);
      setHairUpdate(item);
      setIsVisible(true);
      return;
    }
    return navigation.navigate("Oder", { hair: item, employs: dataEmploys });
  }

  function getColorHeart() {
    let color = "gray";
    let user_like = JSON.parse(dataEmploys.user_like);
    // console.log(dataEmploys);
    if (user_like.length > 0) {
      if (user_like.indexOf(parseInt(dataUser.id)) != -1) {
        color = "red";
      }
    }
    return color;
  }

  function updateUserLike(user_like, like, idEmploys) {
    let params = {
      user_like: JSON.stringify(user_like),
      like: like,
    };
    let link = API.employs + `/${idEmploys}`;
    fetch(link, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    }).then((response) => response.json())
      .then(async res => {
        if (res.id) {
          dispatch(updateEmploys(res));
        } else {
          flashMessageShowError("Có lỗi xảy ra!");
        }
      }).catch(e => {
      flashMessageShowError(e);
    }).finally(() => setLoading(false));
  }
}

const styles = StyleSheet.create({
  imageAvt: {
    width: 100,
    height: 100,
    borderRadius: 20,
    marginTop: -50,
    borderWidth: 3,
    borderColor: "white",
  },
  container: {
    backgroundColor: "white",
    borderTopLeftRadius: 50,
    marginTop: -50,
    flex: 1,
  },
  textStyle: {
    fontWeight: "700",
    color: Color.button,
    fontSize: 16,
  },
  btn: {
    width: WIDTH_SCREEN / 3,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    backgroundColor: Color.button,
    borderRadius: 24,
  },
});
