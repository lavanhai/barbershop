import React, { useEffect, useState } from "react";
import { FlatList, StatusBar, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import ItemView from "../ViewUtils/ItemView";
import TextView from "../ViewUtils/TextView";

function Like() {
  const dataUser = useSelector(state => state.user.dataUser);
  const allEmploys = useSelector(state => state.employs.dataAllEmploys);
  const navigation = useNavigation();
  const [employsUserLike, setEmploysUserLike] = useState([]);

  useEffect(() => {
    let arr = [];
    allEmploys.forEach(employs => {
      let userLike = JSON.parse(employs.user_like);
      let id_user = parseInt(dataUser.id);
      if (userLike.indexOf(id_user) != -1) {
        arr.push(employs);
      }
    });
    setEmploysUserLike(arr);
  }, [allEmploys]);

  return (
    <View style={{ flex: 1, backgroundColor: "#63C2D1", paddingTop: StatusBar.currentHeight }}>
      {employsUserLike.length == 0 ?
        <View style = {{alignItems: 'center'}}>
          <TextView text={"Bạn chưa thả tim ai"} style={{ color: "white", fontSize: 18}} />
        </View>

        : <FlatList
          data={employsUserLike}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => {
            return (
              <ItemView
                image={{ uri: item.image }}
                name={item.name}
                onPress={() => navigation.navigate("DetailEmploys", { employs: item })}
                textButton={"Chi Tiết"}
                titleStatus={"Like: " + item.like}
              />
            );
          }}
        />
      }
    </View>
  );
}

export default Like;
