import React, { useEffect, useState } from "react";
import { Agenda } from "react-native-calendars";
import { Alert, Image, Text, View } from "react-native";
import { useSelector } from "react-redux";
import moment from "moment";
import TextView from "../ViewUtils/TextView";
import { getUserByID } from "../Users/handleUser";
import { CANCEL, DOING, DONE, getHairByID, getStatusBill, UpdateDoneOder } from "../Oder/handleOder";
import { getEmployByID, updateEmploys } from "../Employs/handleEmploy";
import { PERMISSION_ADMIN, PERMISSION_EMPLOYS, PERMISSION_USER } from "../../Define/const";
import { TouchableOpacity } from "react-native-gesture-handler";

let imgSize = 80;


export default function Calendar() {
  const [items, setItems] = useState({});
  const dataOder = useSelector(state => state.oder.dataOder);
  const dataUser = useSelector(state => state.user.dataUser);

  useEffect(() => {
    getData();
  }, [dataOder]);
  return (
    <View style={{ flex: 1 }}>
      <Agenda
        items={items}
        selected={moment().format("YYYY-MM-DD")}
        renderItem={(item) => renderItems(item)}
        renderEmptyData={() => renderEmptyData()}
        onDayPress={(item) =>
          <View><TextView text={"abcaldhalkdna"} /></View>}
      />
    </View>
  );

  function checkPermissionAdmin() {
    if (dataUser) {
      switch (dataUser.permission) {
        case PERMISSION_ADMIN:
          return true;
        default:
          return false;
      }
    }
  }

  function renderItems(item) {
    return (
      <TouchableOpacity
        onPress={() => showAlertDeleteOder(item)}
        style={{
          backgroundColor: "white",
          flex: 1,
          borderRadius: 5,
          padding: 10,
          marginRight: 10,
          marginTop: 17,
          height: 150,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}>
        <View>
          <TextView text={"Nhân viên: " + item.employ.name} />
          <TextView text={"Kiểu tóc: " + item.hair.name} />
          <TextView text={"Giá: " + item.price + " VNĐ"} />
          <TextView text={"Thời gian: " + item.time} />
          <TextView text={"Người đặt lịch: " + item.user.name} />
          {checkPermissionAdmin() ?  <TextView text={"Trạng thái: " + getStatusBill(item.done)}/>: null}
        </View>
        <Image
          style={{ width: imgSize, height: imgSize, marginLeft: 20 }}
          source={{ uri: item.employ.image }} />
      </TouchableOpacity>
    );
    // }
  }

  function renderEmptyData() {
    return (
      <View style={{
        alignItems: "center",
        backgroundColor: "white",
        borderRadius: 5,
        padding: 10,
        marginTop: 17,
      }}>
        <Text>Hôm nay bạn rảnh!</Text>
      </View>
    );
  }

  function getData() {
    let data = dataOder;
    // console.log(dataOder);
    if (dataOder && dataOder.length > 0) {
      switch (dataUser.permission) {
        case PERMISSION_EMPLOYS:
          data = dataOder.filter(oder => oder.employId == dataUser.employsId && oder.done == DOING);
          break;
        case PERMISSION_USER:
          data = dataOder.filter(oder => oder.userId == dataUser.id && oder.done == DOING);
          break;
        default:
          data = dataOder;
          break;
      }
      let arr_day = [];
      data.forEach(value => {
        let day = value.date.split(" ")[0];
        if (arr_day.indexOf(day) == -1) {
          arr_day.push(day);
        }
      });
      let items = {};
      arr_day.forEach(day => {
        let data_items = [];
        data.forEach(oder => {
          let day_oder = oder.date.split(" ")[0];
          let time = oder.date.split(" ")[1];
          if (day_oder == day) {
            data_items.push({
              id: oder.id,
              day: day_oder,
              time: time,
              price: oder.price,
              user: getUserByID(oder.userId),
              hair: getHairByID(oder.hairId),
              employ: getEmployByID(oder.employId),
              done:oder.done
            });
          }
        });
        items[day] = data_items;
      });
      setItems(items);
    }
  }

  function showAlertDeleteOder(item) {
    switch (dataUser.permission) {
      case PERMISSION_EMPLOYS:
        showAlertEmploys(item);
        break;
      case PERMISSION_USER:
        showAlertUser(item);
        break;
      default:
        return;
    }
  }

  function showAlertEmploys(item) {
    Alert.alert(
      "Hoàn thành / Huỷ lịch",
      "Bạn có chắc đã hoàn thành hoặc muốn huỷ lịch không?",
      [
        {
          text: "Huỷ",
          style: "cancel",
        },
        { text: "Huỷ lịch", onPress: () => UpdateDoneOder(item,CANCEL) },
        {
          text: "Hoàn thành", onPress: () => employsComplete(item),
        },
      ],
    );
  }

  function showAlertUser(item) {
    Alert.alert(
      "Huỷ lịch",
      "Bạn có chắc muốn huỷ lịch không?",
      [
        {
          text: "Huỷ",
          style: "cancel",
        },
        { text: "Huỷ lịch", onPress: () => UpdateDoneOder(item, CANCEL) },
      ],
    );
  }

  function employsComplete(item) {
    let sumOder = item.employ.sumOder;
    let sumMoney = item.employ.sumMoney;
    let params = {
      sumOder: sumOder + 1,
      sumMoney: parseInt(sumMoney) + parseInt(item.price),
    };
    updateEmploys(item.employ.id, params);
    UpdateDoneOder(item, DONE)
  }
}
