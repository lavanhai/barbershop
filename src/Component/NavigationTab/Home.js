import React, { useState } from "react";
import { FlatList, StatusBar, Text, TextInput, TouchableOpacity, View, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import { Picker } from "native-base";
import { ListEmploys } from "../Employs/ListEmploys";
import { useNavigation } from "@react-navigation/native";
import { PERMISSION_ADMIN, PERMISSION_EMPLOYS, WIDTH_SCREEN } from "../../Define/const";
import ItemView from "../ViewUtils/ItemView";
import TextView from "../ViewUtils/TextView";
import ModalCustom from "../../ViewCustom/ModalCustom";
import { Color } from "../../Common/Color";
import { flashMessageShowWarning } from "../../FunctionCustom/FlashMessageShow";
import { updateHair } from "../Hair/ListHair";
import moment from "moment";
import { getSumBillToDay, getSumMoneyToDay } from "../Oder/handleOder";

export default function Home() {
  const dataUser = useSelector(state => state.user.dataUser);
  const allEmploys = useSelector(state => state.employs.dataAllEmploys);
  const dataHair = useSelector(state => state.hair.dataHair);
  const [isVisible, setIsVisible] = useState(false);
  const [nameHair, setNameHair] = useState("");
  const [priceHair, setPriceHair] = useState("");
  const [hairUpdate, setHairUpdate] = useState(null);
  const dataOder = useSelector(state => state.oder.dataOder);
  const navigation = useNavigation();

  const [address, setAddress] = useState("0");
  return (
    <View style={{ flex: 1, backgroundColor: "#63C2D1", paddingTop: StatusBar.currentHeight }}>
      {renderBodyHome()}
      <ModalCustom
        title={"Sửa thông tin"}
        isVisible={isVisible}
        onBackdropPress={() => setIsVisible(false)}
      >
        <View style={{ paddingHorizontal: 10 }}>
          <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
            <View style={{ flex: 3 }}>
              <Text>Tên kiểu tóc</Text>
            </View>
            <TextInput
              value={nameHair}
              onChangeText={(text) => setNameHair(text)}
              placeholder={"Nhập tên kiểu tóc"}
              style={{ height: 40, flex: 7 }}
            />
          </View>
          <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10, borderBottomWidth: 0.3 }}>
            <View style={{ flex: 3 }}>
              <Text>Giá</Text>
            </View>
            <TextInput
              value={priceHair}
              onChangeText={(text) => setPriceHair(text)}
              placeholder={"Nhập giá"}
              style={{ height: 40, flex: 7 }}
            />
          </View>
          <View
            style={{ alignItems: "center", marginTop: 30, flexDirection: "row", justifyContent: "space-evenly" }}>
            <TouchableOpacity
              onPress={() => setIsVisible(false)}
              style={styles.btn}>
              <TextView text={"Hủy"} style={{ color: "white" }} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => saveUpdateHair()}
              style={styles.btn}>
              <TextView text={"Lưu lại"} style={{ color: "white" }} />
            </TouchableOpacity>
          </View>
        </View>
      </ModalCustom>
    </View>
  );

  function saveUpdateHair() {
    if (nameHair.length == 0 || priceHair.length == 0) {
      return flashMessageShowWarning("Dữ liệu không được để trống");
    }
    let params = {
      name: nameHair,
      price: priceHair,
    };
    updateHair(hairUpdate.id, params);
    setIsVisible(false);
  }

  function renderBodyHome() {
    if (dataUser) {
      switch (dataUser.permission) {
        case PERMISSION_ADMIN:
          return <PermissionAdmin />;
        case PERMISSION_EMPLOYS:
          return <PermissionEmploy />;
        default:
          return <PermissionUser />;
      }
    }
  }

  function PermissionEmploy() {
    return (
      <FlatList
        data={dataHair}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <ItemView
              image={{ uri: item.image }}
              name={item.name}
              onPress={() => {
                setIsVisible(true);
                setHairUpdate(item);
                setNameHair(item.name);
                setPriceHair(item.price);
              }}
              textButton={"Sửa"}
              titleStatus={"Giá: " + item.price + " VND"}
            />
          );
        }}
      />
    );
  }

  function PermissionAdmin() {
    let date = moment(new Date()).format("DD/MM/YYYY");
    return (
      <View style = {{flex: 1}} >
        <View style={{alignItems :'center',marginBottom: 20}}>
          <Text>Ngày: {date}</Text>
          <Text>Tổng số đơn: {getSumBillToDay()}</Text>
          <Text>Tổng sô tiền: {getSumMoneyToDay()}</Text>
        </View>
        <FlatList
          data={allEmploys}
          keyExtractor={(item) => item.id}
          renderItem={({ item, index }) => {
            return (
              <ItemView
                image={{ uri: item.image }}
                name={item.name}
                onPress={() => navigation.navigate("DetailEmploys", { employs: item, key: "admin" })}
                textButton={"Chi Tiết"}
                sumOder={"Tổng số đơn hoàn thành: " + item.sumOder}
                sumMoney={"Tổng số tiền: " + item.sumMoney + (item.sumMoney > 0 ? "đ" : "")}
                titleStatus={"Yêu thích: " + item.like}
              />
            );
          }}
        />
      </View>
    );
  }

  function PermissionUser() {
    return (
      <View style={{ flex: 1, paddingBottom: 50 }}>
        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-evenly" }}>
          <View>
            <Text style={{ fontSize: 16 }}>Chi nhánh</Text>
          </View>
          <View>
            <Picker
              selectedValue={address}
              style={{ height: 50, width: 180 }}
              onValueChange={(itemValue) => setAddress(itemValue)}
            >
              <Picker.Item label="Tất cả" value="0" />
              <Picker.Item label="Thái Nguyên" value="1" />
              <Picker.Item label="Hà Nội" value="2" />
              <Picker.Item label="Bắc Ninh" value="3" />
              <Picker.Item label="Bắc Giang" value="4" />
            </Picker>
          </View>
        </View>
        <ListEmploys
          address={address}
          data={allEmploys}
          onPress={(item) => {
            navigation.navigate("DetailEmploys", { employs: item });
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  btn: {
    width: WIDTH_SCREEN / 3,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    backgroundColor: Color.button,
    borderRadius: 24,
  },
});
