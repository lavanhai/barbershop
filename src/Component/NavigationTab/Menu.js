import React, { useState, useEffect } from "react";
import { StatusBar, View, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ACCOUNT, WIDTH_SCREEN } from "../../Define/const";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { setUser } from "../../Redux/Actions/userAction";
import showDialog from "../../ViewCustom/Dialog";
import TextView from "../ViewUtils/TextView";
import { ItemMenu } from "../ViewUtils/ItemMenu";
import Modal from "react-native-modal";
import { API } from "../../Common/API";
import { flashMessageShowSuccess, flashMessageShowWarning } from "../../FunctionCustom/FlashMessageShow";
import { Color } from "../../Common/Color";
import ModalCustom from "../../ViewCustom/ModalCustom";
import { getDataAllUser } from "../Account/Splash";


export default function Menu() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.dataUser);
  const [isVisible, setIsVisible] = useState(false);
  const [isVisiblePass, setIsVisiblePass] = useState(false);
  const [newName, setNewName] = useState(user ? user.name : "");
  const [oldPass, setOldPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [reNewPass, setReNewPass] = useState("");

// *********************************** CHANGE NAME *******************************************

  function dataChangeName(name) {
    let params = {
      name: name,
    };
    let link = API.user + `/${user.id}`;
    // console.log("link", link);ư
    fetch(link, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    }).then(response => response.json())
      .then(res => {
        if (res) {
          if (newName !== "") {
            params.name = newName;
            setNewName(params.name);
            setIsVisible(false);
            dispatch(setUser(res));
            AsyncStorage.setItem(ACCOUNT, JSON.stringify(res));
            flashMessageShowSuccess("Thay đổi tên thành công!");
          } else {
            flashMessageShowWarning("Không được bỏ trống tên");
          }
        }
      });
  }

  function changeName() {
    dataChangeName(newName);
  }

  //  ************************************ FINISH CHANGE NAME **************************************

  //  ************************************ CHANGE PASSWORD *****************************************

  function dataChangePass(password) {
    let params = {
      password: password,
    };
    let link = API.user + `/${user.id}`;
    fetch(link, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    }).then(response => response.json())
      .then(res => {
        getDataAllUser();
        dispatch(setUser(res));
        AsyncStorage.setItem(ACCOUNT, JSON.stringify(res));
        setIsVisiblePass(false);
        flashMessageShowSuccess("Đổi mật khẩu thành công");
      });
  }

  function changePassword() {
    if (oldPass.length == 0 || newPass.length == 0 || reNewPass.length == 0) {
      return flashMessageShowWarning("Không được bỏ trống mật khẩu");
    }
    if (oldPass != user.password) {
      return flashMessageShowWarning("Nhập sai mật khẩu cũ");
    }
    if (newPass != reNewPass) {
      return flashMessageShowWarning("Mật khẩu nhập lại không giống nhau");
    }
    return dataChangePass(newPass);
  }

  //  ************************************ FINISH PASSWORD *****************************************

  if (user) {
    return (
      <View style={{ flex: 1, backgroundColor: "#63C2D1", paddingTop: StatusBar.currentHeight }}>
        {/*<Header title={"Menu"} />*/}
        <View style={{ padding: 10, flex: 1, justifyContent: "center", marginLeft: 30 }}>
          <TextView
            style={{ fontSize: 26, color: "white", fontWeight: "700" }}
            text={user.name} />
          <TextView
            style={{ fontSize: 18, color: "white" }}
            text={user.phone} />
        </View>
        <View style={{ margin: 10, backgroundColor: "white", borderRadius: 16, flex: 5 }}>
          <View style={{ padding: 10 }}>

            <ItemMenu
              onPress={() => {
                setIsVisible(true);
              }}
              icon={"account-circle"}
              text={"Đổi tên"}
            />
            <ItemMenu
              onPress={() => {
                setIsVisiblePass(true);
              }}
              icon={"lock"}
              text={"Đổi mật khẩu"}
            />

            <ItemMenu
              onPress={() => showDialog("Đăng xuất", "Bạn có muốn đăng xuất không?", handleLogout)}
              icon={"logout-variant"}
              text={"Đăng xuất"}
            />
          </View>
        </View>

        <ModalCustom
          isVisible={isVisiblePass}
          title={"Đổi mật khẩu"}
          onBackdropPress={() => setIsVisiblePass(false)}
        >
          <View style={{ paddingHorizontal: 20, marginTop: 10 }}>
            <TextView text={"Mật khẩu cũ"} />
            <TextInput
              style={styles.input}
              onChangeText={(t) => setOldPass(t)}
              // value={newName}
              secureTextEntry={true}
              placeholder="Mật khẩu cũ"
            />

          </View>
          <View style={{ paddingHorizontal: 20, marginTop: 10 }}>
            <TextView text={"Mật khẩu mới"} />
            <TextInput
              style={styles.input}
              onChangeText={(x) => setNewPass(x)}
              secureTextEntry={true}
              // value={newName}
              placeholder="Mật khẩu mới"
            />

          </View><View style={{ paddingHorizontal: 20, marginTop: 10 }}>
          <TextView text={"Nhập lại mật khẩu mới"} />
          <TextInput
            style={styles.input}
            secureTextEntry={true}
            onChangeText={(y) => setReNewPass(y)}
            // value={newName}
            placeholder="Nhập lại mật khẩu mới"
          />

        </View>


          <View style={{ alignItems: "flex-end", paddingTop: 10 }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <TouchableOpacity
                onPress={() => setIsVisiblePass(false)}
              >
                <TextView
                  style={{ color: "black", fontSize: 16, marginRight: 10 }}
                  text={"Thoát"} />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  padding: 10, backgroundColor: Color.button,
                  borderTopLeftRadius: 30, borderBottomLeftRadius: 30, width: WIDTH_SCREEN / 3,
                }}
                onPress={() => {
                  changePassword();
                }}>
                <TextView
                  style={{ color: "white", fontSize: 18, paddingLeft: 20 }}
                  text={"Lưu"} />
              </TouchableOpacity>
            </View>
          </View>
        </ModalCustom>
        <Modal
          avoidKeyboard={true}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          backdropOpacity={0.2}
          backdropColor={"#253b5a"}
          isVisible={isVisible}
          onBackdropPress={() => setIsVisible(false)}
        >
          <View style={{
            backgroundColor: "#fff",
            width: "100%",
            paddingVertical: 20,
            borderRadius: 14,
          }}>
            <View style={{ padding: 20 }}>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <TextView
                  style={{
                    fontWeight: "700", fontSize: 16,
                  }}
                  text={"Đổi tên".toUpperCase()} />
              </View>
              <TextView text={"Tên"} />
              <TextInput
                style={styles.input}
                onChangeText={(t) => setNewName(t)}
                value={newName}
                placeholder="Nhập tên"
              />
            </View>
            <View style={{ alignItems: "flex-end", paddingTop: 10 }}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity
                  onPress={() => setIsVisible(false)}
                >
                  <TextView
                    style={{ color: "black", fontSize: 16, marginRight: 10 }}
                    text={"Thoát"} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    padding: 10, backgroundColor: Color.button,
                    borderTopLeftRadius: 30, borderBottomLeftRadius: 30, width: WIDTH_SCREEN / 3,
                  }}
                  onPress={() => {
                    changeName();

                  }}
                >
                  <TextView
                    style={{ color: "white", fontSize: 18, paddingLeft: 20 }}
                    text={"Lưu"} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  } else {
    return <View />;
  }

  function handleLogout() {
    AsyncStorage.removeItem(ACCOUNT);
    dispatch(setUser(null));
    navigation.replace("Login");
  }
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: "white", borderWidth: 1,
    borderColor: "#E4E4E4", borderRadius: 4, padding: 8, marginTop: 10,
  },
});
