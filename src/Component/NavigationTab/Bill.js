import React from "react";
import { FlatList, ScrollView, Text, View } from "react-native";
import { useSelector } from "react-redux";
import { getEmployByID } from "../Employs/handleEmploy";
import { getHairByID, getStatusBill } from "../Oder/handleOder";

export default function Bill() {
  const dataOder = useSelector(state => state.oder.dataOder);
  return (
    <ScrollView>
     <View style={{alignItems:'center', marginVertical:10}}>
       <Text style={{fontWeight:'bold'}}>Thống kê lịch sử</Text>
     </View>
      <FlatList
        data={dataOder}
        inverted={true}
        renderItem={({ item }) => {
          return (
            <View style={{margin:10, borderWidth:0.3, padding:15}}>
              <Text>Ngày: {item.date}</Text>
              <Text>Nhân viên: {getEmployByID(item.employId).name}</Text>
              <Text>Kiểu tóc: {getHairByID(item.hairId).name}</Text>
              <Text>Giá: {item.price}đ</Text>
              <Text>Trạng thái: {getStatusBill(item.done)}</Text>
            </View>
          );
        }}
      />
    </ScrollView>
  );
}
