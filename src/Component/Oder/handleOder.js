import store from "../../Redux/Store";
import { API } from "../../Common/API";
import { setOder } from "../../Redux/Actions/oderAction";
import { flashMessageShowSuccess } from "../../FunctionCustom/FlashMessageShow";
import moment from "moment";

export const CANCEL = "1";
export const DONE = "2";
export const DOING = "3";

export function getHairByID(id_hair) {
  let dataAllHair = store.getState().hair.dataHair;
  let hair = dataAllHair.filter(value => value.id === id_hair)[0];
  if (hair) {
    return hair;
  }
  return [];
}

export function UpdateDoneOder(item, typeDone) {
  const dataOder = store.getState().oder.dataOder;
  let params = {
    done: typeDone,
  };
  fetch(API.oders + `/${item.id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(params),
  }).then((response) => response.json())
    .then((res) => {
      if (res.id) {
        dataOder.filter(oder => {
          if (oder.id == res.id) {
            oder.done = typeDone;
          }
        });
        store.dispatch(setOder(dataOder.slice()));
        return flashMessageShowSuccess("Thành công!");
      }
      return flashMessageShowSuccess("Thất bại!");
    }).catch(e => {
    console.log("err", e);
  });
}

export function getDataOderToday() {
  const dataOder = store.getState().oder.dataOder;
  let date = moment(new Date()).format("YYYY-MM-DD");
  let dataOderToday = [];
  if (dataOder && dataOder.length > 0) {
    dataOder.forEach(value => {
      let dateOder = value.date.split(" ")[0];
      if (dateOder == date && value.done == DONE) {
        dataOderToday.push(value);
      }
    });
  }
  return dataOderToday;
}

export function getSumBillToDay() {
  let dataOderToday = getDataOderToday();
  return dataOderToday.length;
}

export function getSumMoneyToDay() {
  let dataOderToday = getDataOderToday();
  let money = 0;
  if (dataOderToday.length > 0) {
    dataOderToday.forEach(value => {
      money += parseInt(value.price);
    });
  }
  return money;
}

export function getStatusBill(status){
  switch (status){
    case DONE:
      return "Đã hoàn thành"
    case CANCEL:
      return "Đã huỷ"
    default:
      return "Chưa hoàn thành"
  }
}
