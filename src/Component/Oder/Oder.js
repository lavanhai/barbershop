import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, TouchableOpacity, View } from "react-native";
import Header from "../../ViewCustom/Header";
import { useNavigation } from "@react-navigation/native";
import ItemView from "../ViewUtils/ItemView";
import RNDateTimePicker from "@react-native-community/datetimepicker";
import TextView from "../ViewUtils/TextView";
import moment from "moment";
import { useSelector } from "react-redux";
import { Button } from "../../ViewCustom/Button";
import { API } from "../../Common/API";
import {
  flashMessageShowError,
  flashMessageShowSuccess,
  flashMessageShowWarning,
} from "../../FunctionCustom/FlashMessageShow";
import { getAllDataOder } from "../Account/Splash";
import { DOING } from "./handleOder";

export default function Oder(props) {
  const navigation = useNavigation();
  let { hair } = props.route.params;
  let { employs } = props.route.params;
  const dataHair = useSelector(state => state.hair.dataHair);
  const dataUser = useSelector(state => state.user.dataUser);
  const dataEmploys = useSelector(state => state.employs.dataAllEmploys);
  const dataOder = useSelector(state => state.oder.dataOder);
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const [day, setDay] = useState(``);
  const [time, setTime] = useState(``);
  const [min, setMin] = useState(``);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
    // console.log('date', date);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };

  useEffect(() => {
    let day = `${moment(date).format("YYYY-MM-DD")}`;
    let time = `${moment(date).format("HH")}`;
    let min = `${moment(date).format("mm")}`;
    setDay(day);
    setTime(time);
    setMin(min);
  }, [date]);

  function handleOder() {
    let userID = dataUser.id;
    let hairId = hair.id;
    let employId = employs.id;
    let price = hair.price;
    let date = day + " " + time + ":" + min;
    let check = true;

    var now = moment(new Date()); //todays date
    var end = moment(date); // another date
    var dur = moment.duration(end.diff(now));
    let m = dur.asMinutes();
    if(m <= 0){
      return flashMessageShowWarning("Vui lòng chọn lại thời gian!");
    }
    if (time > 7 && time < 20) {
      if (dataOder.length > 0) {
        dataOder.forEach(value => {
          let d = value.date.split(" ")[0];
          if (employId == value.employId && value.done == DOING && d == day) {
            let start = moment(value.date); //todays date
            let end = moment(date); // another date
            let duration = moment.duration(end.diff(start));
            let minutes = Math.abs(parseInt(duration.asMinutes()));
            if (minutes <= 30) {
              check = false;
            }
          }
        });
        if (check) {
          navigation.goBack();
          updateOder(userID, hairId, employId, price, date);
        } else {
          return flashMessageShowWarning("Thời gian này đã có người đặt vui lòng chọn thời gian khác!");
        }
      } else {
        navigation.goBack();
        updateOder(userID, hairId, employId, price, date);
      }
    } else {
      return flashMessageShowWarning("Thời gian nhân viên nghỉ ngơi");
    }
  }

  function updateOder(userId, hairId, employId, price, date) {
    let params = {
      userId: userId,
      hairId: hairId,
      employId: employId,
      price: price,
      date: date,
      done: DOING,

    };
    let link = API.oders;
    // console.log('params', params);
    fetch(link, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    }).then((response) => response.json())
      .then(res => {
        if (res.id) {
          getAllDataOder();
          return flashMessageShowSuccess("Đặt lịch thành công");
        }
        return flashMessageShowError("Đặt lịch không thành công");
      });

  }


  return (
    <View style={{ flex: 1, backgroundColor: "#63C2D1" }}>
      <Header
        onBack={() => navigation.goBack()}
        title={"Đặt lịch"} />
      <ItemView
        image={{ uri: hair.image }}
        name={hair.name}
        titleStatus={hair.price}
      />


      <View style={styles.viewDate}>
        <TouchableOpacity
          style={styles.btnDate}
          onPress={showDatepicker}>
          <TextView text={day} />
        </TouchableOpacity>


        <TouchableOpacity
          style={styles.btnDate}
          onPress={showTimepicker}>
          <TextView text={time + ":" + min} />
        </TouchableOpacity>
      </View>
      {/*<TextView text={`${moment(date).format("DD-MM-YYYY h:mm:ss")}`} />*/}

      <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center", paddingBottom: 10 }}>
        <Button
          onPress={() => {
            handleOder();
          }}
          text={"Đặt lịch"}
        />
      </View>


      {show && (
        <RNDateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}

    </View>
  );
}
const styles = StyleSheet.create({
  viewDate: {
    borderRadius: 16,
    width: "100%",
    marginVertical: 5,
    paddingHorizontal: 15,
  },
  btnDate: {
    flexDirection: "row",
    paddingVertical: 20,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 10,
    borderRadius: 16,
  },
});
