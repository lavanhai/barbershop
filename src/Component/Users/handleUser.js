import store from "../../Redux/Store";
export function getUserByID(id_user) {
  let dataAllUser = store.getState().user.dataAllUser;
  let user = dataAllUser.filter(value => value.id == id_user)[0];
  if (user) {
    return user;
  }
  return [];
}
