import { showMessage } from "react-native-flash-message";

export class KEY_TYPE_FLASH_MESSAGE {
  static SUCCESS = "success";
  static DANGER = "danger";
  static WARNING = "warning";
}


export const flashMessageShowNotification = (description = "", message = "Có lỗi xảy ra", type = KEY_TYPE_FLASH_MESSAGE.DANGER, ref = null) => {
  let data = {
    message: message,
    description: description,
    type: type,
    duration: 2000,
    icon: "auto",
  };
  if (ref == null) {
    showMessage(data);
  } else {
    ref.current.showMessage(data);
  }
};

export const flashMessageShowError = (description = "", ref = null) => {
  flashMessageShowNotification(description, "Lỗi", KEY_TYPE_FLASH_MESSAGE.DANGER, ref);
};

export const flashMessageShowSuccess = (description = "", ref = null) => {
  flashMessageShowNotification(description, "Thành công", KEY_TYPE_FLASH_MESSAGE.SUCCESS, ref);
};
export const flashMessageShowWarning = (description = "", ref = null) => {
  flashMessageShowNotification(description, "Cảnh báo", KEY_TYPE_FLASH_MESSAGE.WARNING, ref);
};
