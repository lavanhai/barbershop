import { Dimensions } from "react-native";

export const WIDTH_SCREEN = Dimensions.get("window").width;
export const HEIGHT_SCREEN = Dimensions.get("window").height;
export const REMEMBER_PASSWORD = "REMEMBER_PASSWORD";


//Storage
export const ACCOUNT = 'ACCOUNT';

//Permission
export const PERMISSION_USER = 2;
export const PERMISSION_EMPLOYS = 1;
export const PERMISSION_ADMIN = 0;
