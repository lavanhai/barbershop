import { SET_DATA_ODER } from "../Reducers/oderReducer";

export function setOder(data) {
  return { type: SET_DATA_ODER, data };
}
