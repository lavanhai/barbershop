import { SET_DATA_ALL_EMPLOYS, UPDATE_EMPLOYS, USER_LIKE_EMPLOYS } from "../Reducers/employsReducer";

export function setAllEmploys(data) {
  return { type: SET_DATA_ALL_EMPLOYS, data };
}
export function updateEmploys(data) {
  return { type: UPDATE_EMPLOYS, data };
}
export function setDataUserLike(data) {
  return { type: USER_LIKE_EMPLOYS, data };
}
