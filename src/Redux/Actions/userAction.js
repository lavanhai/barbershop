import { INIT_USER, SET_DATA_ALL_USER } from "../Reducers/userReducer";

export function setUser(data) {
  return { type: INIT_USER, data };
}
export function setAllUser(data) {
  return { type: SET_DATA_ALL_USER, data };
}
