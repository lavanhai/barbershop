import { SET_DATA_HAIR } from "../Reducers/hairReducer";

export function setDataHair(data) {
  return { type: SET_DATA_HAIR, data };
}
