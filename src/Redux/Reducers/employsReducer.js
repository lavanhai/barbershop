const defaultState = {
  dataAllEmploys: null,
};

export const SET_DATA_ALL_EMPLOYS = "SET_DATA_ALL_EMPLOYS";
export const UPDATE_EMPLOYS = "UPDATE_EMPLOYS";
export const USER_LIKE_EMPLOYS = "USER_LIKE_EMPLOYS";

const employsReducer = (state = defaultState , action) => {
  switch (action.type) {
    case SET_DATA_ALL_EMPLOYS:
      return { ...state, dataAllEmploys: action.data };
    case UPDATE_EMPLOYS:
      state.dataAllEmploys.forEach((value, index) => {
        if (value.id == action.data.id) {
          state.dataAllEmploys.splice(index, 1, action.data);
        }
      })
      return {
        ...state,
        dataAllEmploys: state.dataAllEmploys.slice(),
      };
    case USER_LIKE_EMPLOYS:
      return { ...state, dataAllEmploys: action.data };

    default:
      return state;
  }
};
export default employsReducer;
