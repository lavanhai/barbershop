const defaultState = {
  dataUser: null,
  dataAllUser: null,
};
export const INIT_USER = "INIT_USER";
export const SET_DATA_ALL_USER = "SET_DATA_ALL_USER";
const userReducer = (state = defaultState, action) => {
  switch (action.type) {
    case INIT_USER:
      return { ...state, dataUser: action.data };
    case SET_DATA_ALL_USER:
      return { ...state, dataAllUser: action.data };
    default:
      return state;
  }
};
export default userReducer;
