import { combineReducers } from "redux";
import userReducer from "./userReducer";
import employsReducer from "./employsReducer";
import hairReducer from "./hairReducer";
import oderReducer from "./oderReducer";

const reducer = combineReducers({
  user: userReducer,
  employs: employsReducer,
  hair:hairReducer,
  oder: oderReducer,
});
export default reducer;
