const defaultState = {
  dataOder: null
}
export const SET_DATA_ODER = "SET_DATA_ODER";

const oderReducer = (state = defaultState, action) => {
  switch (action.type){
    case SET_DATA_ODER:
      return { ...state, dataOder: action.data }
    default:
      return state;
  }
}

export default oderReducer;
