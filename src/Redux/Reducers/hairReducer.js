const defaultState = {
  dataHair: null,
};
export const SET_DATA_HAIR = "SET_DATA_HAIR";

const hairReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_DATA_HAIR:
      return { ...state, dataHair: action.data };
    default:
      return state;
  }
};
export default hairReducer;
