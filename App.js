import React from "react";
import { Provider, useSelector } from "react-redux";
import store from "./src/Redux/Store";
import { LogBox } from "react-native";

LogBox.ignoreLogs(["Warning: ..."]); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications

import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import Home from "./src/Component/NavigationTab/Home";
import Menu from "./src/Component/NavigationTab/Menu";
import Calendar from "./src/Component/NavigationTab/Calendar";
import Login from "./src/Component/Account/Login";
import Register from "./src/Component/Account/Register";
import Splash from "./src/Component/Account/Splash";
import Oder from "./src/Component/Oder/Oder";
import Bill from "./src/Component/NavigationTab/Bill";

import OneSignal from "react-native-onesignal";
import { FlashMessageView } from "./src/ViewCustom/FlashMessageView";
import { Color } from "./src/Common/Color";
import { DetailEmploys } from "./src/Component/Employs/DetailEmploys";
import Like from "./src/Component/NavigationTab/Like";
import { PERMISSION_ADMIN, PERMISSION_EMPLOYS, PERMISSION_USER } from "./src/Define/const";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function NavTab() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          if (route.name === "Home") {
            if (focused) {
              return <Icon name={"home-circle"} color={Color.button} size={30} />;
            } else {
              return <Icon name={"home-circle"} color={"gray"} size={30} />;
            }
          } else if (route.name === "Menu") {
            if (focused) {
              return <Icon name={"menu"} color={Color.button} size={30} />;
            } else {
              return <Icon name={"menu"} color={"gray"} size={30} />;
            }
          } else if (route.name === "Calendar") {
            if (focused) {
              return <Icon name={"calendar-check"} color={Color.button} size={30} />;
            } else {
              return <Icon name={"calendar-check"} color={"gray"} size={30} />;
            }
          } else if (route.name === "Like") {
            if (focused) {
              return <Icon name={"heart-circle"} color={Color.button} size={30} />;
            } else {
              return <Icon name={"heart-circle"} color={"gray"} size={30} />;
            }
          }else if (route.name === "Bill") {
            if (focused) {
              return <Icon name={"book-open-outline"} color={Color.button} size={30} />;
            } else {
              return <Icon name={"book-open-outline"} color={"gray"} size={30} />;
            }
          }

        },
      })}
      tabBarOptions={{
        showLabel: false,
      }}
    >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Calendar" component={Calendar} />
      {checkPermissionAdmin() ?<Tab.Screen name="Bill" component={Bill} /> : null}
      {checkPermissionUser() ? <Tab.Screen name="Like" component={Like} /> : null}
      <Tab.Screen name="Menu" component={Menu} />

    </Tab.Navigator>
  );
}

function checkPermissionUser() {
  const dataUser = useSelector(state => state.user.dataUser);
  if (dataUser) {
    switch (dataUser.permission) {
      case PERMISSION_USER:
        return true;
      default:
        return false;
    }
  }
}
function checkPermissionAdmin() {
  const dataUser = useSelector(state => state.user.dataUser);
  if (dataUser) {
    switch (dataUser.permission) {
      case PERMISSION_ADMIN:
        return true;
      default:
        return false;
    }
  }
}

function StackContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Splash" component={Splash} options={{ headerTitle: null, headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerTitle: null, headerShown: false }} />
        <Stack.Screen name="Register" component={Register} options={{ headerTitle: null, headerShown: false }} />
        <Stack.Screen name="NavTab" component={NavTab} options={{ headerTitle: null, headerShown: false }} />
        <Stack.Screen name="DetailEmploys" component={DetailEmploys}
                      options={{ headerTitle: null, headerShown: false }} />
        <Stack.Screen name="Oder" component={Oder} options={{ headerTitle: null, headerShown: false }} />
        {/*<Stack.Screen name="Bill" component={Bill} options={{ headerTitle: null, headerShown: false }} />*/}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default class App extends React.Component {
  componentDidMount() {
    OneSignal.setAppId("95fe0cc1-b0a4-4225-aca8-55423bffd99b");
    OneSignal.setLogLevel(6, 0);
    OneSignal.setRequiresUserPrivacyConsent(false);
  }

  render() {
    return (
      <Provider store={store}>
        {StackContainer()}
        <FlashMessageView />
      </Provider>
    );
  }
}
